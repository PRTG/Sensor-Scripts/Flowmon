This blog article explains how to integrate Flowmon’s digital performance & security suite with PRTG - 

https://blog.paessler.com/perfect-monitoring-combine-prtgs-breadth-with-flowmons-depth

The article describes how you can use a set of custom sensors to retrieve monitoring data from a Flowmon device. The script sensors were developed by Flowmon and were made available through their customer portal. Flowmon have kindly allowed us to share them here as well.

Two files are included – a ZIP archive containing the scripts, and a PDF document containing instructions.
If needed, additional information can be found here - 

https://kb.paessler.com/en/topic/88023-how-can-i-integrate-flowmon-into-prtg-network-monitor#reply-306331


